import React from 'react'
import "./Footer.css"
export default function Footer() {
  return (
    <footer className='themeFooter' style={{backgroundColor:"#03111b"}}>

    <div className='footerPadding text-light'>
      <div className="container">
          <div className="row ">
            <div className="col-4">
              <h1 className='footerTitle font-bold'>Acount</h1>
              <div className="footerContent">
                <p>My Account</p>
                <p>Watchlist</p>
                <p>Collection</p>
                <p>User Guide</p>
              </div>
            </div>
            <div className="col-4">
              <h1  className='footerTitle font-bold'>Resources</h1>
              <div className="footerContent">
                <p>Forums</p>
                <p>Blog</p>
                <p>Help Center</p>
              </div>
            </div>
            <div className="col-4">
              <h1  className='footerTitle font-bold'>Legal</h1>
              <div className="footerContent">
                <p>Terms of Use</p>
                <p>Privacy Policy</p>
                <p>Security</p>
              </div>
            </div>
          </div>
      </div>
      
    </div>
    </footer>
  )
}
