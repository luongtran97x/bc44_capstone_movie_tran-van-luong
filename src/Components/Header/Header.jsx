import React from "react";
import "./Header.css";
import { NavLink } from "react-router-dom";
export default function Header() {
  return (
    <div className="headerColor">
      <div className="container">
        <div className="container flex justify-between space-y-5 p-3">
          <h1 className="text-red-500 text-3xl shadow font-bold">
            <NavLink to="/">Cyber NetFlix</NavLink>
            </h1>
        </div>
      </div>
      
    </div>
  );
}
